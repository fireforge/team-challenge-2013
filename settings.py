# Car Hues (construction paper colors)
CAR1_HUES = [120,   185]
CAR2_HUES = [90,  125]
CAR3_HUES = [24,  40]
CAR4_HUES = [50,  80]

# Color rgb
RGB = { "Yellow": [0,  255,255],
        "Green": [0,  255,0],
        "Blue": [255,0,  0],
        "Red": [0,  0,  255]}
        
# Screen position indicators
TOPRIGHT = 'topRight'
TOPLEFT = 'topLeft'
BOTTOMRIGHT = 'bottomRight'
BOTTOMLEFT = 'bottomLeft'

# These dicts set will hold all the parameters for the game
# Note: "Car IDs" are actually the hue ranges
TEAM_INFO = {
    "Red Team": {
        "Color": "Red",
        "Base Position": TOPRIGHT, # Box in the form of (x, y, w, h). x,y position is relative to the frame origin
        "Cars": {
            "Car 1": CAR1_HUES}, # Hue value range in the form of (h_lower, h_upper).
        "Points": 0
        },
    "Blue Team": {
        "Color": "Blue",
        "Base Position": TOPLEFT, # Box in the form of (x, y, w, h).
        "Cars": {
            "Car 1": CAR2_HUES},
        "Points": 0
        },
    "Yellow Team": {
        "Color": "Yellow",
        "Base Position": BOTTOMRIGHT, # Box in the form of (x, y, w, h).
        "Cars": {
            "Car 1": CAR3_HUES},
        "Points": 0
        },
    "Green Team": {
        "Color": "Green",
        "Base Position": BOTTOMLEFT, # Box in the form of (x, y, w, h).
        "Cars": {
            "Car 1": CAR4_HUES},
        "Points": 0
        }}

DEFAULTCARINFO = {
    'Hue Vals': [],
    'Position': (0,0),
    'PrevPosition': (),
    'HasFlag': False,
    'TeamName': ''}

GAME_SETTINGS = {
    "Max Flags": 2, # Note: "Flag IDs" are actually the positions
    "Flag Margin": 30,
    "Car Margin": 30,
    "Base Flag Radius": 40,
    "BaseLength": 150,
    "FlagSpawnMargin": 120,
    "FlagRespawnTime": 30 # seconds between flag respawns
    }