import cv2
import numpy as np
from game import game, settings

SAT = [160, 255]
VAL = [200, 255]
carSize = 30

# Color rgb
YELLOW_RGB = [0,   255, 255]
GREEN_RGB  = [0,   255, 0]
BLUE_RGB   = [255, 0,   0]
RED_RGB    = [0,   0,   255]
BLACK_RGB  = [255, 255, 255]
WHITE_RGB  = [0,   0,   0]

EDIT_HUE = settings.CAR3_HUES
# hueMin = EDIT_HUE[0];
# hueMax = EDIT_HUE[1];
# satMin = SAT[0];
# satMax = SAT[1];
# valMin = VAL[0];
# valMax = VAL[1];

def drawbase(f, teaminfo):
    h,w,_ = f.shape
    length = settings.GAME_SETTINGS["BaseLength"]
    
    basebox = ()
    if teaminfo['Base Position'] == settings.TOPRIGHT:
        basebox = (w-length, 0, length, length)
    elif teaminfo['Base Position'] == settings.TOPLEFT:
        basebox = (0, 0, length, length)
    elif teaminfo['Base Position'] == settings.BOTTOMRIGHT:
        basebox = (w-length, h-length, length, length)
    elif teaminfo['Base Position'] == settings.BOTTOMLEFT:
        basebox = (0, h-length, length, length)
    
    drawboundingbox(f, basebox[0], basebox[1], basebox[2], basebox[3], settings.RGB[teaminfo['Color']])
    cv2.putText(f,
        str(teaminfo['Points']), # Text to display
        (basebox[0]+length/2-20, basebox[1]+length/2), # Bottom left corner of the text
        cv2.FONT_HERSHEY_SIMPLEX,
        1, # Scale
        (0,0,0) # Color
        )
        

def getthresholdedimg(hsv, h_pair):
    thresh = cv2.inRange(
        hsv,
        np.array((h_pair[0], SAT[0], VAL[0])),
        np.array((h_pair[1], SAT[1], VAL[1])))
    return thresh
    
def checkcolorinframe(hsv, coord, h_pair):
    hueIdx = 0
    return h_pair[0] < hsv.item(coord[1], coord[0], hueIdx) < h_pair[1]
    
def drawboundingbox(f, x, y, h, w, color):
    cv2.rectangle(f, (x,y), (x+w,y+h), color, 5)
    
def drawhitbox(f, rect, color, size):
    cv2.rectangle(f, (rect[0]+rect[2]/2+size,rect[1]+rect[3]/2+size),
        (rect[0]+rect[2]/2-size,rect[1]+rect[3]/2-size), color, -1)
    cv2.rectangle(f, (rect[0]+rect[2]/2+size,rect[1]+rect[3]/2+size),
        (rect[0]+rect[2]/2-size,rect[1]+rect[3]/2-size), (0,0,0), 1)
    
def drawflag(f, x, y):
    cv2.circle(f, (x, y), 5, WHITE_RGB, -1)
    cv2.circle(f, (x, y), 5, BLACK_RGB)

def changeMinHue(x, hue=EDIT_HUE):
    hue[0] = x
#     hueMin = x
   
def changeMaxHue(x, hue=EDIT_HUE):
    hue[1] = x
#     hueMax = x

def changeMinSat(x):
    SAT[0] = x
#     satMin = x
   
def changeMaxSat(x):
    SAT[1] = x
#     satMax = x

def changeMinVal(x):
    VAL[0] = x
#     valMin = x
   
def changeMaxVal(x):
    VAL[1] = x
#     valMax = x


def main():
    c = cv2.VideoCapture(0)
    width,height = c.get(3),c.get(4)
    print "frame width and height : ", width, height
    
    # Create game object
    g = game(height, width)
    
    cv2.namedWindow('sliders')
    cv2.createTrackbar('HueMin','sliders',EDIT_HUE[0],255,changeMinHue)
    cv2.createTrackbar('HueMax','sliders',EDIT_HUE[1],255,changeMaxHue)
    cv2.createTrackbar('SatMin','sliders',SAT[0],255,changeMinSat)
    cv2.createTrackbar('SatMax','sliders',SAT[1],255,changeMaxSat)
    cv2.createTrackbar('ValMin','sliders',VAL[0],255,changeMinVal)
    cv2.createTrackbar('ValMax','sliders',VAL[1],255,changeMaxVal)
    cv2.resizeWindow('sliders', 800, 600)
    
    while(1):
        ## Image Input
        _,f = c.read() # get frame
        f = cv2.transpose(f)
        f = cv2.flip(f, 1)
        blur = cv2.medianBlur(f,5) # Low pass filter
        hsv = cv2.cvtColor(f,cv2.COLOR_BGR2HSV) # Convert to HSV for color segmentation
        
        for teamname in g.getTeamNames():
            teaminfo = g.getTeamInfo(teamname)
            basebox = drawbase(f, teaminfo)
            base = g.getBaseFlagCircle(teamname)
            cv2.circle(f, (int(base[0]),int(base[1])), base[2], (0,0,0)) 
        
        for carIdx, currentCarInfo in enumerate(g.getCarInfo()):
            both = getthresholdedimg(hsv, currentCarInfo['HueVals'])
            cv2.imshow('threshold' + str(currentCarInfo['TeamName']),both)
            
            # Erosion and dilation to pick out constant areas of color
            erode = cv2.erode(both, None, iterations = 3)
            dilate = cv2.dilate(erode, None, iterations = 10)
            
            ## Finds cars in frame
            # Find contours
            contours, _ = cv2.findContours(dilate,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
            if contours:
                contourAreas = [cv2.contourArea(contour) for contour in contours]
                largestContour = contours[contourAreas.index(max(contourAreas))]

                x,y,w,h = cv2.boundingRect(largestContour)
                cx,cy = x+w/2, y+h/2
                
                # Get team color
                teamcolor = settings.RGB[g.getTeamInfo(currentCarInfo['TeamName'])['Color']]
                g.setCarPosition(carIdx, (cx,cy)) # Save location info for this car to game state
                pos = g.getCarRect(carIdx)
                drawhitbox(f, pos, teamcolor, carSize)
            else:
                g.setCarPosition(carIdx,())
        
        
        
        
        ## Game Drawing and Update
        g.update() # Call game's update function (handle creation and deletion of flags, point gain, etc.)
        
        # TODO: Draw static flag return areas
        for flagPos in g.getFlagIds(): # Draw flags
            drawflag(f, flagPos[0], flagPos[1])
        
        ## Apply Changes to Frame
        cv2.imshow('img',f)
        
        ## Handle keyboard input
        if cv2.waitKey(25) == 27:
            break

    cv2.destroyAllWindows()
    c.release()
    
if __name__ == '__main__':
    main()