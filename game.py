import itertools
from collections import namedtuple
import random
from copy import deepcopy
import time
import math

import settings

FrameRes = namedtuple('FrameRes', ['w', 'h'])

class game:
    """
    A game object which keep track of the state of a game. The game consists of multiple teams each 
    with muliple cars moving around an area with virtual "flags" that the cars collect for points.
    """
    def __init__(self, frame_width, frame_height):
        """
        Initializes the game object with the teams given in team info and general game rules in gameinfo.
             
        resolution: (width, height)
        """
        self.frameRes = FrameRes(w=frame_width, h=frame_height)
        self.teamInfo = settings.TEAM_INFO
        self.prevtime = 0
        
        # Get an iterator over all the team's car hue lists
        teamCarHues_iter = (y.values() for y in (x['Cars'] for x in self.teamInfo.values())) 
        carTeams_iter = (k for k,v in self.teamInfo.items() for y in v['Cars'])
        carHues_iter = itertools.chain(*teamCarHues_iter) # Create list of all car hues
        
        # Create a dict for all the cars to hold their info
        self.carInfo = []
        for huevals, teamname in itertools.izip(carHues_iter, carTeams_iter):
            info = deepcopy(settings.DEFAULTCARINFO)
            info['HueVals'] = huevals
            info['TeamName'] = teamname
            self.carInfo.append(info)
        
        # Initialize the positions of all the flags
        self.flagmargin = settings.GAME_SETTINGS["Flag Margin"]
        self.flags = [] # Setting flags as an empty list for 
        self.flags = [self._getNewFlagPos() for x in range(settings.GAME_SETTINGS["Max Flags"])]
        
    def update(self):
        """
        Updates the state of the game based on the positions of the cars in this frame
        """
        currenttime = time.time()
        
        for currentCarInfo in self.carInfo:
            if currentCarInfo['Position'] is (): # If the car is not tracked
                if currentCarInfo['HasFlag']:
                    currentCarInfo['HasFlag'] = False # Remove flag
                    if currentCarInfo['PrevPosition'] is not (): # If the car has a valid previous position, create a flag there
                        self.flags.append(currentCarInfo['PrevPosition'])
            else:
                teamname = currentCarInfo['TeamName']
                ## Scoring: Check to see if the car can drop off their flag
                if self._inBase(teamname, currentCarInfo['Position']) and currentCarInfo['HasFlag']:
                    self.teamInfo[teamname]['Points'] += 1 # Give the team a point
                    currentCarInfo['HasFlag'] = False # Remove flags from cars
                
                ## Stealing: Check to see if the car can steal a flag from a base
                otherteams = self.getTeamNames()
                otherteams.remove(teamname) # Can't steal from your own base
                for team in otherteams:
                    otherteaminfo = self.teamInfo[team]
                    if self._onBaseFlag(team, currentCarInfo['Position']) and \
                    not currentCarInfo['HasFlag'] and \
                    (otherteaminfo['Points'] > 0):
                        # Put a new flag on the thief
                        currentCarInfo['HasFlag'] = True
                        self.teamInfo[team]['Points'] -= 1 # Deduct a point from the victim
                
                ## PickUp: Check to see if the car can pick up a flag
                flag = self._onFlag(currentCarInfo['Position'])
                if not currentCarInfo['HasFlag'] and flag:
                    del self.flags[self.flags.index(flag)]
                    currentCarInfo['HasFlag'] = True
                
        ## CleanUp: Score flags on bases
        flagsToRemove = []
        for flagpos in self.flags:
            for teamname in self.getTeamNames():
                if self._inBase(teamname, flagpos):
                    self.teamInfo[teamname]['Points'] += 1 # Give the team a point
                    flagsToRemove.append(self.flags.index(flagpos))
        for flagidx in flagsToRemove:
            del self.flags[flagidx]
                
        print self.flags
        print [car['HasFlag'] for car in self.carInfo]
        
        ## AddFlags: See if there are more flags needed
        if (self.prevtime + settings.GAME_SETTINGS['FlagRespawnTime']) < currenttime and len(self.flags) < settings.GAME_SETTINGS['Max Flags']:
            self.flags.append( self._getNewFlagPos() ) # Create a new flag
        
            self.prevtime = currenttime
        
    def setCarPosition(self, caridx, positon):
        """
        Set a car's position by entering it's car ID.
        position: A tuple like (X, Y)
        """
        self.carInfo[caridx]['PrevPosition'] = self.carInfo[caridx]['Position']
        self.carInfo[caridx]['Position'] = positon
        
    def getCarInfo(self):
        """
        Get a list of info on the cars based on settings.py
        """
        return self.carInfo
        
    def getFlagIds(self):
        """
        Get a list of flag IDs for the flags in the game
        """
        flaggedCarPositions = [car['Position'] for car in self.carInfo if car['HasFlag']]
        return self.flags + flaggedCarPositions
        
    def getTeamNames(self):
        """
        Get a list of team names in the game based on settings.py
        """
        return self.teamInfo.keys()
        
    def getTeamInfo(self, teamname):
        """
        Get a dict of info on the given team based on settings.py
        """
        return self.teamInfo[teamname]
        
    def getCarRect(self, caridx):
        carinfo = self.carInfo[caridx]
        return self._getCarRectFromPos(carinfo['Position'])
        
    def getBaseRect(self, teamname):
        return self._getBaseRectFromInfo(self.teamInfo[teamname])
        
    def getBaseFlagCircle(self, teamname):
        return self._getBaseFlagCircleFromInfo(self.teamInfo[teamname])
        
    def _getCarRectFromPos(self, pos):
        margin = settings.GAME_SETTINGS["Car Margin"]/2
        halfmargin = margin/2
        return [pos[0]-halfmargin, pos[1]-halfmargin, margin, margin]
        
    def _getBaseRectFromInfo(self, teaminfo):
        w = self.frameRes.w
        h = self.frameRes.h
        length = settings.GAME_SETTINGS['BaseLength']
        if teaminfo['Base Position'] == settings.TOPRIGHT:
            return (w-length, 0, length, length)
        elif teaminfo['Base Position'] == settings.TOPLEFT:
            return (0, 0, length, length)
        elif teaminfo['Base Position'] == settings.BOTTOMRIGHT:
            return (w-length, h-length, length, length)
        elif teaminfo['Base Position'] == settings.BOTTOMLEFT:
            return (0, h-length, length, length)
            
    def _getBaseFlagCircleFromInfo(self, teaminfo):
        length = settings.GAME_SETTINGS['BaseLength']
        baserect = self._getBaseRectFromInfo(teaminfo)
        xc = baserect[0] + length/2
        yc = baserect[1] + length/2
        return (xc, yc, settings.GAME_SETTINGS["Base Flag Radius"])
        
    def _randomFlagPos(self, max):
        return random.randrange(0 + self.flagmargin, max - self.flagmargin)
        
    def _nearFlag(self, pos):
        # TODO: Give a bit of room around the flag to not place other flags in
        return pos in self.flags
        
    def _onFlag(self, pos):
        carRect = self._getCarRectFromPos(pos)
        for flagPos in self.flags:
            if self._inRect(carRect, flagPos):
                return flagPos
        return ()
        
    def _getNewFlagPos(self):
        pos = (self._randomFlagPos(self.frameRes.w), self._randomFlagPos(self.frameRes.h))
        centerRect = self._getCenterRect()
        while not self._inRect(centerRect, pos) or self._nearFlag(pos):
            pos = (self._randomFlagPos(self.frameRes.w), self._randomFlagPos(self.frameRes.h))
            centerRect = self._getCenterRect()
            
        return pos
        
    def _inBase(self, teamname, pos):
        baserect = self._getBaseRectFromInfo(self.teamInfo[teamname])
        return self._inRect(baserect, pos)
        
    def _onBaseFlag(self, teamname, pos):
        baseflag = self._getBaseFlagCircleFromInfo(self.teamInfo[teamname])
        return self._inCircle(baseflag, pos)
        
    def _getCenterRect(self):
        margin = settings.GAME_SETTINGS['FlagSpawnMargin']
        return (margin,
                margin,
                self.frameRes.w - margin*2,
                self.frameRes.h - margin*2)
        
    def _inRect(self, rect, pos):
        return rect[0] <= pos[0] < rect[0]+rect[2] and rect[1] <= pos[1] < rect[1]+rect[3]
        
    def _inCircle(self, circle, pos):
        return math.sqrt((circle[0] - pos[0])**2 + (circle[1] - pos[1])**2) < circle[2]
        
if __name__ == '__main__':
    random.seed(0) # So the random positions are the same everytime when testing
    g = game(640.0, 480.0)
    print(dir(g))
    g.setCarPosition(1, (5,6))
    print('CARINFO:', g.getCarInfo())
    teams = g.getTeamNames(); print(teams)
    print(g.getTeamInfo(teams[0]))
    flags = g.getFlagIds(); print(flags)